"""
Autor: Trung Ngoc Dao, 485686
"""

from random import randint, random


def game(length, bias, output, special_tile, jump):
    """
    Function game:
    Simulation of one game. Returns the sum of strokes needed to end the game.
    length - number of game tiles.
    bias - probability of throwing 6 on a dice.
    output - True if input should be printed on screen, False otherwise.
    special_tile - Specifies which tiles move player.
    jump - indicates how many tiles will player move after stepping on special tile
    """

    Turn = 0
    position = 0
    p_player = "#"
    for _ in range(length):
        p_player += "."
        
    while position < length:
        throw = dice(bias)
        Turn += 1
        
        while position + throw > length:
            if output:
                print("Turn: ", Turn, "Throws: ", throw, "Position: ", position)
                print(p_player)
            throw = dice(bias)
            Turn += 1
        position += throw
        
        if output:
            print("Turn:", Turn, "Throws:", throw, "Position: ", end=" ")
            
        if position + jump <= length and (position) % special_tile == 0:
            position += jump
            throw += jump
            jump = length
            
        if output:
            print(position, end=" ")
            if jump == length:
                print("JUMP!")
                jump += 1
            else:
                print()
            p_player = p_player[:(position - throw)] + "." + p_player[(position - throw + 1):position] + "#" + p_player[(position + 1):]
            print(p_player)
            
    if output:
        print("Game finisheds in turn {} ".format(Turn))
    return Turn


def game_repetitions(length, bias, output, special_tile, jump, rep):
    """
    Function game_repetitions:
    Repeats the "game" n - times.
    """

    total = 0
    for i in range(rep):
        total += game(length, bias, output, special_tile, jump)
    return total


def game_average_length(bias, output, special_tile, jump, k=100, repetitions=100):
    """
    Function game_average_length:
    Prints information about average game length.
    Run this analysis for length of game plan from 2 tiles to k tiles.

    k - maximal length of the game plan. Default is 100 tiles.
    repet. - The number of repetitions played at a given length. Default is 100.
    """

    if output:
        print(game(k, bias, output, special_tile, jump))
    else:
        for j in range(2, k + 1):
            x = game_repetitions(j, bias, output, special_tile, jump, repetitions)
            total_avg = x / repetitions
            round(total_avg, 2)
            print("Plan length: ", j, "->", "Avg. turns: ", total_avg)


def analyse_bias(length, special_tile, jump, k=100, repetitions=100):
    """
    Function analyse_bias:
    Counts and prints information about optimal bias of the dice.

    k - length of the game plan. Default is 100 tiles.
    repetitions - The number of repetitions played at a given length. Default is 100.
    """

    mini = float("inf")   # Nastavenie na + nekonečno
    for k in range(0, 96, 5):
        x = game_repetitions(length, k / 100, False, special_tile, jump, repetitions)
        total_avg = x / repetitions
        round(total_avg, 3)
        if total_avg < mini:
            mini = total_avg
            best_bias = k
        print("Bias: ", k / 100, "->", "Avg. turns: ", total_avg)
    print("Best bias: {}".format(best_bias / 100))


def dice(bias):
    """
    Funciton dice:
    Rolls the dice.
    """

    if round(random(), 2) > bias:
        return randint(1, 5)
    return 6

# game(44, 0.3, True, 5, 2)
# game_average_length(0.95, False, 5, 2)
# analyse_bias(100, 5, 2)
