"""
Autor: Trung Ngoc Dao, 485686
"""

"""
Co su divne piskvorky

Divné piškvorky (zatím jenom pracovní název) jsou hra, kterou moj cvicaci
vy-myslel, když se jednoho víkendového odpoledne asi dva roky zpátky nudil.
Hra se podobá normálním piškvorkám tím,
že se hraje na mřížce a vepisujíse X a O (tím podobnost končí).
"""

"""
Pravidla
•Střídají se dva hráči v pokládání X a O.
•První tah může být kamkoliv.
•Všechny další tahy musí sousedit hranou s obsazeným políčkem.
•Hráč vyhraje, když má v řádku nebo sloupci dva své znaky oddělenéjedním
nebo více prázdnými políčky.
"""
from random import random, randint


def print_board(board):
    """
    Prints the board with numbers,...
    """
    print(end="   ")
    
    for x in range(len(board[0])):
        print(x, end=" ")
        
    print()
    print(end="   ")

    for x in range(len(board[0])):
        print("-", end=" ")
        
    print()
    
    for y in range(len(board)):
        print(y, end="| ")
        for x in range(len(board[0])):
            print(board[y][x], end=" ")
        print()


def game(width, height, player1, player2):
    """
    Handles single game of player1 vs. player2.

    :param width: width of a playing board
    :param height: height of a playing board
    :param player1: "human"/"computer", determines whether player1 is human or computer
    :param player2: "human"/"computer", determines whether player1 is human or computer

    :return: number of player who won, 0 if it's draw
    """

    board = [["." for _ in range(width)] for _ in range(height)]
    print_board(board)

    i = 0
    won = 0
    while won == 0:  # checks if they are stil playing
        sumx = 0
        for row in board:
            sumx += row.count(".")
        print()
        print(sumx)
        print()
        if sumx == 0:
            break
        if i % 2 == 0:  # checks who is on turn
            won = move(board, 0)
        else:
            won = move(board, 1)
        i += 1
    print("Winner is player number ", won)


def move(board, player_n):
    """
    This function decides who is playing
    """
    if players[player_n] == "human":
        won = player_move(board, player_n)
    elif players[player_n] == "computer":
        won = computer_move(board, player_n)
    return won


def is_valid_position(board, x, y):
    """
    This function checks wheter the move is valid
    """
    if y >= len(board) or x >= len(board[y]) or x < 0 or y < 0:  # Checking whether index out of range
        return False
    started = False  # Checks if the first move has been made
    for row in board:
        if "X" in row:
            started = True
            break
    if started:
        if board[y][x] == ".":
            if y-1 >= 0 and board[y-1][x] != ".":  # Checking validity
                return True
            elif y+1 < len(board) and board[y+1][x] != ".":
                return True
            elif x-1 >= 0 and board[y][x-1] != ".":
                return True
            elif x+1 < len(board[y]) and board[y][x+1] != ".":
                return True
            return False
        else:
            return False
    else:
        return True


def computer_move(board, number):
    """
    Returns coordinates of random legal move.

    :param board: list of lists representing playing board
    :param number: number of player to play

    :return: (x, y), coordinates of tile, MUST be legal move
    """
    choosing = True
    while choosing:
        x = randint(0, len(board[0])-1)
        y = randint(0, len(board)-1)
        if is_valid_position(board, x, y):
            board[y][x] = "O"
            choosing = False
    print_board(board)
    return who_won(board, x, y, number)


def player_move(board, number):
    """
    Asks user to input coordinates. Makes sure it's legal move.

    :param board: list of lists representing playing board
    :param number: number of player to play

    :return: (x, y), coordinates of tile, MUST be legal move
    """
    x = int(input("Zadaj suradnicu x "))
    y = int(input("Zadaj suradnicu y "))
    print()
    while not is_valid_position(board, x, y,):
        print("Nevalidne suradnice")
        x = int(input("Zadaj suradnicu x "))
        y = int(input("Zadaj suradnicu y "))
        
    if number == 0:
        print("Player1 turn: ", x, y)
        board[y][x] = "X"
    elif number == 1:
        print("Player2 turn: ", x, y)
        board[y][x] = "O"
    won = who_won(board, x, y, number)
    print_board(board)
    return won


def who_won(board, x, y, number):
    """
    Determines who won on given board. Assume there is at most one winner.

    :param board: list of lists representing playing board

    :return: number of player who won, 0 if there is no winner
    This function checks the space around the charackter , left ,right, top, bottom side
    """
    symbol = board[y][x]
    for i in range(x-1, 0, -1):
        if i < x-1:
            if board[y][i] == symbol:
                return number + 1
        if board[y][i] != ".":
            break
    for i in range(x+1, len(board[x])):
        if i > x+1:
            if board[y][i] == symbol:
                return number + 1
        if board[y][i] != ".":
            break

    for j in range(y-1, 0, -1):
        if j < y-1:
            if board[j][x] == symbol:
                return number + 1
        if board[j][x] != ".":
            break

    for j in range(y+1, len(board)):
        if j > y+1:
            if board[j][x] == symbol:
                return number + 1
        if board[j][x] != ".":
            break
    return 0


# players = ["human", "human"]
players = ["human", "computer"]
game(10, 10, players[0], players[1])
