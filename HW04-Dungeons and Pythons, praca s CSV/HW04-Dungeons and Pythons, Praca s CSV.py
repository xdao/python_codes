"""
Autor: Trung Ngoc Dao, 485686
"""
from random import randint


class Player:
    
    def __init__(self):
        self.name = self.asking_name()
        self.HP = 100
        self.midHP = 100
        self.EP = 100
        self.midEP = 100
        self.equipped_weapon = self.generate_weapon()
        self.skills = self.hero_type()

    def get_name(self):
        return self.name

    def asking_name(self):
        return input("Please enter your heroic name: ")

    def generate_weapon(self):              #generates deafult weapon for heroes 
        weapon_list = weapon_list_from_csv()
        weapon = weapon_list[randint(0, len(weapon_list) - 1)]
        return weapon

    def hero_type(self):                    #choosing hero wheter mage or hunter
        s = input("Choose character (mage / hunter): ").lower()
        while s != "mage" and s != "hunter":
            s = input("Choose character (mage / hunter): ").lower()
        if s.lower() == "mage":
            return mage_skill_list_from_csv()
        if s.lower() == "hunter":
            return hunter_skill_list_from_csv()

    def __str__(self):
        return self.print_player()

    def print_player(self):                 #returns info about player 
        printer = '\n'
        printer = self.name + ' ' + 'HP: ' + str(self.midHP) + '/' + str(self.HP) + ' EP: ' + str(self.midEP) + '/' + str(self.EP)
        
        return """\n{0}
Your weapon: {1} Damage: {2} Chance of hit: {3}
Your skills:\n {4} \n {5} \n {6}""".format(printer,
                                            self.equipped_weapon.name, self.equipped_weapon.damage,
                                            self.equipped_weapon.probability,
                                            self.skills[0], self.skills[1], self.skills[2])
        
    def given_damage(self):                 #damage given to monster 
        if randint(0, 1) <= self.equipped_weapon.probability:
            return self.equipped_weapon.damage
        else:
            return 0

    def damage_taken(self, given_damage):   #damage given by monster 
        self.midHP -= given_damage
        return self.HP

    def replace_weapon(self):               #replaces  heroes weapon if hero wants it 
        weapons = weapon_list_from_csv()
        weapon = weapons[randint(0, len(weapons) - 1)]
        replace = input("You found {} Damage: {} Chance of hit: {}\n Do you want to equip it? (y / n): ".format(weapon.name, weapon.damage, weapon.probability)).lower()
        while replace not in ['y', 'n']:
            replace = input("You found {} Damage: {} Chance of hit: {}\n Do you want to equip it? (y / n): ".format(weapon.name, weapon.damage, weapon.probability)).lower()
        if replace == 'y':
            self.equipped_weapon = weapon

    def refile_EP(self):
        self.midEP += 10
        if self.midEP > 100:
            self.midEP = 100


class Monster:
    def __init__(self, name, HP, EP, skills):
        self.name = name
        self.HP = HP
        self.midHP = HP
        self.EP = EP
        self.midEP = EP
        self.skills = skills

    def __str__(self):
        return self.print_monster()

    def print_monster(self):                #returns info about monster
        monsters = """\n{0}, HP: {1}/{2}, EP: {3}/{4} \nBandit skills: \n""".format(self.name, self.midHP, self.HP, self.midEP, self.EP)
        skills = self.print_monster_skill()
        return monsters + skills

    def print_monster_skill(self):          #prints monster skills 
        skills = ''
        for i in range(len(self.skills)):
            skills += self.skills[i].print_skills() + '\n'
        return skills

    def skill_used(self):                   #returns used skill
        used_skill = randint(0, 1)        
        self.energy_loss(used_skill)
        return self.skills[used_skill]

    def damage_taken(self, given_damage):
        self.midHP -= given_damage

    def energy_loss(self, skill_used):
        self.midEP -= self.skills[skill_used].energy_cost

    def refile_EP(self):
        self.midEP += 10
        if self.midEP > self.EP:
            self.midEP = self.EP


class Skill:                                #skills of monsters and players 
    def __init__(self, name, damage, energy_cost, probability):
        self.name = name
        self.damage = damage
        self.energy_cost = energy_cost
        self.probability = probability

    def __str__(self):
        return self.print_skills()

    def print_skills(self):
        return """{0}, damage = {1} energy_cost: {2}, probability: {3}""".format(self.name,
                                                                                           self.damage,
                                                                                           self.energy_cost,
                                                                                           self.probability)


class Room: 
    def __init__(self, number):             #number is amount of monster by difficulty
        self.number = number
        self.monsters = self.generate()

    def generate(self):                     #generating number of monsteers into the room
        monsters = []
        monsters_list = monster_list_from_csv() 
        for i in range(self.number):
            chosen = monsters_list[randint(0, len(monsters_list) - 1)]
            monsters.append(chosen)
            monsters_list.remove(chosen)
        return monsters

    def return_monsters(self):              #returns monsters from room
        return self.monsters

    def __str__(self):
        for i in range(len(self.monsters)):
            print(str(i), '-', self.monsters[i])
        return ''


class Weapons:
    def __init__(self, name, damage, probability):
        self.name = name
        self.damage = damage
        self.probability = probability

    def __str__(self):
        return "name = {0}, damage = {1}, probability = {2}".format(self.name, self.damage, self.probability)


class Game:                                 #Gameplay
    def __init__(self):
        self.player = Player()              #Creating our player
        self.dif = self.difficulty()        #e - Easy, m - Medium, h - Hard
        self.rooms = self.fill_rooms()

    def fill_rooms(self):                   #generating room by difficulty
        rooms = []
        if self.dif == 'e':            
            for i in range(4):              
                rooms.append(Room(1))
            return rooms
        elif self.dif == 'm':
            for i in range(6):
                rooms.append(Room(randint(1, 2)))
            return rooms
        elif self.dif == 'h':
            for i in range(8):
                rooms.append(Room(randint(2, 3)))
            return rooms

    def difficulty(self):
        dif = ''
        while dif != 'e' and dif != 'm' and dif != 'h':
            dif = input("Zadajte obtiaznost (e / m / h): ").lower()
        return dif

    def which_monster_to_attack(self):
        number = len(self.rooms[-1].monsters)
        accept = []
        for i in range(number):
            accept.append(i)
        target = int(input("\nPlease select target (number): "))
        while target not in accept:
            target = int(input("\n Please select target (number): "))
        return target                       #target = number of the monster 

    def weapon_or_spell(self):
        use = int(input("Press 0 if you want to attack with your weapon. Press 1 if you want to use your skill: "))
        while use != 0 and use != 1:
            use = int(input("Press 0 if you want to attack with your weapon. Press 1 if you want to use your skill: "))
        return use

    def round(self):                        #Round 
        print(self.player)
        print(self.rooms[-1])               
        target = self.which_monster_to_attack()                                 #number of monster I want to attack
        use = self.weapon_or_spell()
        if use == 1:
            for i in range(len(self.player.skills)):
                print(str(i) + ". ", end="")
                print(self.player.skills[i])
            skill_choose = int(input("Please select your skill (number): "))
            while skill_choose not in [0, 1, 2]:
                skill_choose = int(input("Please select your skill (number): "))
            if randint(0, 1) <= self.player.skills[skill_choose].probability:
                damage = int(self.player.skills[skill_choose].damage)
            else:
                damage = 0
            if damage != 0:
                print("You dealt {} to {}".format(damage, self.rooms[-1].monsters[target].name))
            else:
                print("You missed")
            self.rooms[-1].monsters[target].damage_taken(damage)
            self.player.midEP -= self.player.skills[skill_choose].energy_cost
        else:
            if randint(0, 1) <= self.player.equipped_weapon.probability:
                damage = int(self.player.equipped_weapon.damage)
            else:
                damage = 0
            if damage != 0:
                print("You dealt {} to {}".format(damage, self.rooms[-1].monsters[target].name))
            else:
                print("You missed")
            self.rooms[-1].monsters[target].damage_taken(damage)

        for i in range(len(self.rooms[-1].monsters)):                           #Monster attack
            skill = self.rooms[-1].monsters[i].skill_used()
            if randint(0, 1) < skill.probability:
                self.player.midHP -= skill.damage
                print("{} used {} and dealt {} damage".format(self.rooms[-1].monsters[i].name, skill.name, skill.damage))
            else:
                print("{} used {} and missed".format(self.rooms[-1].monsters[i].name, skill.name))
            self.rooms[-1].monsters[i].refile_EP()
        self.player.refile_EP()

    def game(self):                         #general game function
        while True:
            self.round()
            result = True
            if len(self.rooms) == 0:
                return "You won!"
            elif self.player.midHP <= 0:
                return "You weak, you dead."
                                            # check alive monsters
            for i in range(len(self.rooms[-1].monsters)):
                if self.rooms[-1].monsters[i].midHP > 0:
                    result = False
            if result == True:
                self.rooms.pop()            #move to the next room
                self.player.replace_weapon()
                self.player.midHP += 20
                self.player.midEP = 100
            print("NEXT ROUND")

def weapon_list_from_csv():
    """
    Reading the csv file and creating list of weapons
    """
    list = []
    with open("weapons.csv", "r") as file:
        item = file.readlines()
    for line in item:
        line = line.split(",")
        weapons = Weapons(line[0],int(line[1]),float(line[2]))
        list.append(weapons)
    return list


def monster_skill_list_from_csv():
    """
    Reading csv file and creating list of monster skills 
    """
    list = []
    with open("skills.csv", "r") as file:
        item = file.readlines()
    for line in item:
        line = line.split(",")
        skill = Skill(line[0], int(line[1]), int(line[2]), float(line[3]))
        list.append(skill)
    return list


def mage_skill_list_from_csv():
    """
    Reading csv file and creating background for mage
    """
    list = []
    with open("mage_skills.csv", "r") as file:
        item = file.readlines()
    for line in item:
        line = line.split(",")
        skill = Skill(line[0], int(line[1]), int(line[2]), float(line[3]))
        list.append(skill)
    return list


def hunter_skill_list_from_csv():
    """
    Reading csv file and creating background for hunter
    """
    list = []
    with open("hunter_skills.csv", "r") as file:
        item = file.readlines()
    for line in item:
        line = line.split(",")
        skill = Skill(line[0], int(line[1]), int(line[2]), float(line[3]))
        list.append(skill)
    return list


def monster_list_from_csv():
    """
    Reading csv file and creating list of monsters 
    """
    list = []
    with open("monsters.csv", "r") as file:
        item = file.readlines()    
    for line in item:
        myspells = []
        line = line.split(",") 
        spell_line = line[3].split(";")
        for i in range(len(spell_line)):
            myspells.append(find_spell_byname(spell_line[i]))
        monster = Monster(line[0], int(line[1]), int(line[2]), myspells)
        list.append(monster)
    return list


def find_spell_byname(spellname):
    """
    Reading csv file and  finding skills by name
    """
    with open("skills.csv", "r") as file:
        item = file.readlines()
    for line in item:
        a = line.split(',')
        if a[0] == spellname:
            return Skill(a[0], int(a[1]), int(a[2]), float(a[3]))
    return None


game = Game()
print(game.game())



