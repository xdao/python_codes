"""
Autor: Trung Ngoc Dao, 485686

Prohlašuji, že celý zdrojový kód jsem spracoval zcela samostatně,
Jsem si vědom(a), že nepravdivosť tohoto tvrzení múže být dúvodem
k hodnocení F v prědmetu IB111 a k disciplinárnímu řízení.
"""

from PIL import Image
from itertools import product, permutations
import math


def read_tha_books():
    """
    Analyse books from daScrolls.csv file. Count each of the type.
    :return: List of tuples in format [(type_of_book1, count1), (type_of_book2, count2)..]
    Nedostatky: ziadne
    Programátorsky štýl: v poriadku
 """
    dic = {}
    list_of_tuples = []
    with open("daScrolls.csv", "r") as file:
        item = file.readlines()
    lines_totally = 0
    for line in item:
        line = line.split(";")
        x = line[1]
        lines_totally += 1
        if(x == ""):
            x = "Other"
        if x in dic:
            dic[x] += 1
        else:
            dic[x] = 1
    for key in dic:
        list_of_tuples.append((key, dic[key]))
    return list_of_tuples

# print(read_tha_books())


def analyze_sum_skillz():
    """
    Analyse 'Skill Book' type and 'Spell Tome' type books. Prints graph which visualize count of the books.
    Example of the graph:
    skill tree  count
    conjuration ########################################################
    destruction ###########################################
    speech      ####################
    sneak       ###

    :return: dictionary, where key is the name of the skill tree which is upgraded by the book, value is its count.
    Nedostatky: ziadne
    Programátorsky štýl: v poriadku
    """
    dic = {}
    list_of_tuples = []
    with open("daScrolls.csv", "r") as file:
        item = file.read().split("\n")
        del item[-1]
        for line in item:
            line = line.split(";")
            x = line[1]
            if x == "Skill Book":
                if line[2] in dic:
                    dic[line[2]] += 1
                else:
                    dic[line[2]] = 1
            elif x == "Spell Tome":
                if line[2] in dic:
                    dic[line[2]] += 1
                else:
                    dic[line[2]] = 1
        print("Skill Tree", " ", "      Count = #")
        print()
        for key in dic:
            print(key, end="  ")
            for i in range(dic[key]):
                print("#", end="")
            print()
        print()
        return dic

# print(analyze_sum_skillz())


def inscribe_dha_scroll(background, text):
    """
    Insert the text into background. Both images have the same size.
    :param background: image of the background
    :param text: image of the text
    :return: merged image
    Nedostatky: ziadne
    Programátorsky štýl: v poriadku
    """
    bcg = Image.open(background).convert("RGB")
    txt = Image.open(text).convert("RGB")
    for x in range(900):
        for y in range(1180):
            txt_pixel = txt.getpixel((x, y))
            if txt_pixel == (0, 0, 0):
                bcg.putpixel((x, y), (0, 0, 0))
    merged_image = bcg
    return merged_image

# inscribe_dha_scroll("scroll.png","text.png").show()


def cover_trackz(mightyFrekINGSrulz):
    """
    Blurs the input image
    :param mightyFrekINGSrulz: image
    :return: blurred image
    base = image with border
    n = size of blur
    Nedostatky: chvilu trvalo kym som sa rozhodol ako to idem robit
    Programátorsky štýl: v poriadku
    """
    blurred_image = mightyFrekINGSrulz.convert("RGB")
    width = 900
    height = 1180
    n = 3
    l = list(permutations("0123456", 2))
    base = [[(255, 255, 255) for j in range(height + n * 2)] for i in range(width + n * 2)]
    for i in range(width):                      # in this loop we "insert" the deafult image
        for j in range(height):
            base[i + n][j + n] = blurred_image.getpixel((i, j))
    for i in range(width):
        for j in range(height):
            count = 0
            for k in l:                         # here it counts the sum of the surrounding pixel of red
                count += base[i + int(k[0])][j + int(k[1])][0]
            result_r = count // 42              # 42 = total amount of pixels
            count = 0
            for k in l:                         # here it counts the sum of the surrounding pixel of green
                count += base[i + int(k[0])][j + int(k[1])][1]
            result_g = count // 42
            count = 0
            for k in l:                         # here it counts the sum of the surrounding pixel of blue
                count += base[i + int(k[0])][j + int(k[1])][2]
            result_b = count // 42
            blurred_image.putpixel((i, j), (result_r, result_g, result_b))
    return blurred_image


# cover_trackz(inscribe_dha_scroll("scroll.png","text.png")).show()


class Turtle:
    def __init__(self):
        self.x = 50
        self.y = 50
        self.heading = 0
        self.lines = []
        self.color = (0, 0, 0)

    def left(self, angle):
        self.heading -= angle

    def right(self, angle):
        self.heading += angle

    def forward(self, d):
        nx = self.x + d * math.cos(self.heading * math.pi / 180)
        ny = self.y + d * math.sin(self.heading * math.pi / 180)
        self.lines.append((self.x, self.y, nx, ny))
        self.x, self.y = nx, ny

    def save(self, filename):
        f = open(filename, "w")
        f.write("<svg>")
        s = "<line x1 = '{}' y1 = '{}' x2 = '{}' y2 = '{}' style = '{}' />"
        color = "rgb(" + str(self.color[0]) + ',' + str(self.color[1]) + ',' + str(self.color[2]) + ")"
        for x1, y1, x2, y2 in self.lines:
            f.write(s.format(x1, y1, x2, y2, "stroke:" + color + ";stroke=width:1"))
        f.write("</svg")
        f.close()

    def connect(self, other_turtle):
        self.lines.append((self.x, self.y,
                           other_turtle.x, other_turtle.y))

    def set_color(self, color):
        self.color = color


def pipe():
    t1 = Turtle()
    t2 = Turtle()
    t1.x = 150
    t1.y = 300
    t2.x = 150
    t2.y = 50
    for i in range(360):
        t1.set_color((60, 238, 214))
        t1.forward(2)
        t1.left(2)
        t2.forward(1)
        t2.right(2)
        if i % 3:
            t1.connect(t2)
    t1.save("pipe.svg")


def hourglass():
    t1 = Turtle()
    t2 = Turtle()
    t1.x = 150
    t1.y = 300
    t2.x = 150
    t2.y = 50
    for i in range(360):
        t1.set_color((230, 56, 28))
        t1.forward(2)
        t1.left(2)
        t2.forward(1)
        t2.right(1)
        if i % 3:
            t1.connect(t2)
    t1.save("hourglass.svg")


def horns():
    t1 = Turtle()
    t2 = Turtle()
    t1.x = 150
    t1.y = 300
    t2.x = 150
    t2.y = 50
    for i in range(360):
        t1.set_color((87, 58, 254))
        t1.forward(2)
        t1.left(2)
        t2.forward(1)
        t2.right(1)
        if i % 2 == 0 and (t1.x - 11 > t2.x + 11 or t1.x + 11 < t2.x - 11):
            t1.connect(t2)
    t1.save("horns.svg")

# hourglass()
# horns()
# pipe()
